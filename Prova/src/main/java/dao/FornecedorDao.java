package dao;

import beans.Fornecedor;
import beans.JSONResult;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;

public class FornecedorDao {

	
	public JsonObject Save(Fornecedor f) {

		if (f.getId() > 1) {
			// Fornecedor newF = new Fornecedor();
			// newF = f;
			JSONResult json = new JSONResult(1, "OK", "Operacao de cadastro de novo Fornecedor concluida com exito");
			return json.getJsonResult();
		}

		JSONResult json = new JSONResult(0, "Undone", "Nao pode ser cadastrado porque o Id ja existe");
		return json.getJsonResult();

	}

	public JsonObject Update(Fornecedor f) {
		Fornecedor updatedF = this.GetFornecedorById("1");

		if (updatedF.getId() == f.getId()) {
			updatedF = f;
			JSONResult json = new JSONResult(1, "OK", "Dados atualizados com sucesso");
			return json.getJsonResult();
		} else {
			JSONResult json = new JSONResult(0, "Undone", "Nenhum dado foi atualizado porque o Id nao existe");
			return json.getJsonResult();
		}
		// Operação de atualização do banco usando o "updatedF" como objeto de
		// referência
	}

	public JsonObject Delete(String id) {
		try {
			int fId = Integer.parseInt(id);

			if (fId == 1) {

				JSONResult json = new JSONResult(1, "OK", "Dados deletados com sucesso");
				return json.getJsonResult();
			}

			JSONResult json = new JSONResult(1, "Undone", "Nenhum dado foi deletado porque o Id nao existe");
			return json.getJsonResult();

		} catch (NumberFormatException ex) {

			JSONResult json = new JSONResult(2, "Error",
					"Nenhum dado foi deletado porque o parametro nao e um numero inteiro");
			return json.getJsonResult(); //
		}
	}

	public List<Fornecedor> GetFornecedores() {
		List<Fornecedor> listF = new ArrayList<Fornecedor>();
		Fornecedor f = this.GetFornecedorById("1");
		listF.add(f);
		return listF;
	}

	public Fornecedor GetFornecedorById(String id) {
		Fornecedor f = new Fornecedor();

		try {
			int fId = Integer.parseInt(id);

			if (fId == 1) {
				f.setId(1);
				f.setName("fornec lorimospm");
				f.setEmail("\"fornec@loripsom\"");
				f.setCnpj("00.000/0000-00");
				f.setComment("loreipsum");
			}

		} catch (NumberFormatException ex) {
			// Tratar exceção caso o parâmetro não seja número inteiro.
		}

		return f;
	}

}
