package beans;

public class Fornecedor {

	/*
	 * 
	 * { "id": 1, "name": "fornec lorimospm", "email": "fornec@loripsom", "comment":
	 * "loreipsum", "cnpj": "00.000/0000-00" }
	 * 
	 */

	private int id;
	private String name;
	private String email;
	private String comment;
	private String cnpj;

	public Fornecedor() {

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
}
