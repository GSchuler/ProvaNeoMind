package beans;

import javax.json.Json;
import javax.json.JsonObject;

public class JSONResult {

	private int id;
	private String status;
	private String message;
	private JsonObject result;
	
	public JSONResult(int id, String status, String message) {
		this.id = id;
		this.status = status;
		this.message = message;
		this.result = Json.createObjectBuilder()
				.add("id", this.id)
				.add("status", this.status)
				.add("message", this.message)
				.build();
	}
	
	public JsonObject getJsonResult() {
		return result;
	}

}
