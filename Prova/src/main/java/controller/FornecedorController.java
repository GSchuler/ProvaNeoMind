package controller;

import dao.FornecedorDao;
import beans.Fornecedor;

import java.util.List;

import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.google.gson.Gson;


@Path("/fornecedor")
public class FornecedorController {
	//Esta seria a classe onde a chamada do DAO seria feita, caso o webservice estivesse rodando em um cliente

	FornecedorDao dao = new FornecedorDao();
	Gson gson = new Gson();
	
	@GET
    @Path("/{name}")
    public Response getMsg(@PathParam("name") String name) {
  
        String output = name + " is an invalid method";
  
        return Response.status(200).entity(output).build();
  
    }
	
	@GET
	@Path("save/{id}/{name}/{email}/{comment}/{cnpj}")
	public Response SaveFornecedor(@PathParam("id") String id, @PathParam("name") String name, @PathParam("email") String email, @PathParam("comment") String comment, @PathParam("cnpj") String cnpj)
	{
		Fornecedor f = new Fornecedor();
		f.setId(Integer.parseInt(id));
		f.setName(name);
		f.setEmail(email);
		f.setComment(comment);
		f.setCnpj(cnpj);
		JsonObject result = dao.Save(f);
		String json = gson.toJson(result);
		return Response.status(200).entity(json).build();
	}
	
	@GET
	@Path("update/{id}/{name}/{email}/{comment}/{cnpj}")
	public Response UpdateFornecedor(@PathParam("id") String id, @PathParam("name") String name, @PathParam("email") String email, @PathParam("comment") String comment, @PathParam("cnpj") String cnpj)
	{
		Fornecedor f = new Fornecedor();
		f.setId(Integer.parseInt(id));
		f.setName(name);
		f.setEmail(email);
		f.setComment(comment);
		f.setCnpj(cnpj);
		JsonObject result = dao.Update(f);
		String json = gson.toJson(result);
		return Response.status(200).entity(json).build();
	}
	
	@GET
	@Path("delete/{id}")
	public Response DeleteFornecedor(@PathParam("id") String id)
	{
		JsonObject result = dao.Delete(id);
		String json = gson.toJson(result);
		return Response.status(200).entity(json).build();
	}
	
	@GET
	@Path("listAll")
	public Response ListFornecedores()
	{
		List<Fornecedor> result = dao.GetFornecedores();
		String json = gson.toJson(result);
		return Response.status(200).entity(json).build();
	}
	
	@GET
	@Path("listById/{id}")
	public Response GetFornecedorById(@PathParam("id") String id) 
	{
		Fornecedor result = dao.GetFornecedorById(id);
		String json = gson.toJson(result);
		return Response.status(200).entity(json).build();
	}
}
