package dao;

import org.junit.Test;

import beans.Fornecedor;
import beans.JSONResult;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.json.JsonObject;

public class FornecedorDaoTest {

	private Fornecedor standardF;
	private Fornecedor troubleF;
	private Fornecedor newF;
	private List<Fornecedor> listF;
	private JsonObject jsonSaveOk;
	private JsonObject jsonSaveUndone;
	private JsonObject jsonUpdateOk;
	private JsonObject jsonUpdateUndone;
	private JsonObject jsonDeleteOk;
	private JsonObject jsonDeleteUndone;
	private JsonObject jsonDeleteError;

	public FornecedorDaoTest() {
		standardF = new FornecedorDao().GetFornecedorById("1");
		
		troubleF = new Fornecedor();
		troubleF.setId(0);
		troubleF.setName("bla");
		troubleF.setEmail("bla@bla");
		troubleF.setComment("coBlamenBlat");
		troubleF.setCnpj("123.123.321-22");
		
		newF = new Fornecedor();
		newF.setId(2);
		newF.setName("fornec2");
		newF.setEmail("fornec@2");
		newF.setComment("coment2");
		newF.setCnpj("222.222.222-22");

		listF = new FornecedorDao().GetFornecedores();

		jsonSaveOk = new JSONResult(1, "OK", "Operação de cadastro de novo Fornecedor concluída com êxito")
				.getJsonResult();
		jsonSaveUndone = new JSONResult(0, "Undone", "Não pode ser cadastrado porque o Id já existe").getJsonResult();
		jsonUpdateOk = new JSONResult(1, "OK", "Dados atualizados com sucesso").getJsonResult();
		jsonUpdateUndone = new JSONResult(0, "Undone", "Nenhum dado foi atualizado porque o Id não existe")
				.getJsonResult();
		jsonDeleteOk = new JSONResult(1, "OK", "Dados deletados com sucesso").getJsonResult();
		jsonDeleteUndone = new JSONResult(1, "Undone", "Nenhum dado foi deletado porque o Id não existe")
				.getJsonResult();
		jsonDeleteError = new JSONResult(2, "Error",
				"Nenhum dado foi deletado porque o parâmetro não é um número inteiro").getJsonResult();
	}

	@Test
	public void ListAll() throws Exception {
		final List<Fornecedor> result = new FornecedorDao().GetFornecedores();
		assertThat(result.size(), is(listF.size()));
	}

	@Test
	public void ListById() throws Exception {
		final Fornecedor result = new FornecedorDao().GetFornecedorById("1");
		assertThat(result.getId(), is(standardF.getId()));
	}

	@Test
	public void SaveNew() throws Exception {
		final JsonObject result = new FornecedorDao().Save(newF);
		assertThat(result, is(jsonSaveOk));
	}

	@Test
	public void SaveNewTrouble() throws Exception {
		final JsonObject result = new FornecedorDao().Save(standardF);
		assertThat(result, is(jsonSaveUndone));
	}

	@Test
	public void UpdateExistingId() throws Exception {
		final JsonObject result = new FornecedorDao().Update(standardF);
		assertThat(result, is(jsonUpdateOk));
	}

	@Test
	public void UpdateMissingId() throws Exception {
		final JsonObject result = new FornecedorDao().Update(troubleF);
		assertThat(result, is(jsonUpdateUndone));
	}

	@Test
	public void DeleteExistingId() throws Exception {
		final JsonObject result = new FornecedorDao().Delete("1");
		assertThat(result, is(jsonDeleteOk));
	}

	@Test
	public void DeleteMissingId() throws Exception {
		final JsonObject result = new FornecedorDao().Delete("2");
		assertThat(result, is(jsonDeleteUndone));
	}

	@Test
	public void DeleteTroubleId() throws Exception {
		final JsonObject result = new FornecedorDao().Delete("asd");
		assertThat(result, is(jsonDeleteError));
	}

}
